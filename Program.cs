﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Lecture10
{
    class Program
    {
        private static long Fib(int n)
        {
            if (n < 1)
            {
                throw new ArgumentException();
            }

            if (n == 1)
            {
                return 0;
            }

            if (n == 2)
            {
                return 1;
            }

            return Fib(n - 1) + Fib(n - 2);
        }

        private static void DeleteDirectory(string path)
        {
            var files = Directory.EnumerateFiles(path);
            var directories = Directory.EnumerateDirectories(path);

            foreach (var file in files)
            {
                File.Delete(file);
            }

            foreach (var directory in directories)
            {
                DeleteDirectory(directory);
            }

            if (!Directory.EnumerateFileSystemEntries(path).Any())
            {
                Directory.Delete(path);
            }
        }

        static void Main(string[] args)
        {
            DeleteDirectory("Lvl1");

            //В строке найти первое вещественное число, вывести на экран
            const string numbers = "a, d ,2010asda-3.158, 74 , -5, wqe";
            var regExp = new Regex(@"[-]?[\d+][.][\d+]{1,}"); //Вещественным число считаем только представленное в виде целая_часть.дробная часть
            var m = regExp.Match(numbers);
            Console.WriteLine("Input: " + numbers);
            Console.WriteLine("Found: " + m.Value);
            Console.ReadKey();
            Console.WriteLine("------------------");

            //Найти в строке все даты, вывести на экран. Даты задаются в формате “15 Oct 2012”
            const string dates = "2022 15 oct 2012, august 16th, 2022 15 Oct 2012, 13.03.2011 16 Oct 2012 sadasdasd23 Aug 1993";
            regExp = new Regex(@"[\d]{2}[ ]{1}[JFMASOND]{1}[aepuco]{1}[nbrylgptvc]{1}[ ]{1}[\d]{4}");
            Console.WriteLine("Input: " + numbers);
            var matches = regExp.Matches(dates);
            foreach (Match match in matches)
            {
                Console.WriteLine("Found: " + match.Value);
            }
            Console.ReadKey();
            Console.WriteLine("------------------");

            //Написать функцию, вычисляющее N-ое число Фибоначчи
            var n = 40;
            Console.WriteLine("{0}-е фисло Фибоначчи: {1}", n, Fib(n));
            Console.ReadKey();
            Console.WriteLine("------------------");
        }
    }
}
